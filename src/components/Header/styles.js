import styled from 'styled-components'

export const Container  = styled.div`

  height: 5.4rem;
  background: #20295F;
  border-bottom: 5px solid #EE6b26;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;

`

export const Editlogo = styled.div`

background: #fff;
width: 100px;
border-radius: 50%;
display: flex;
align-items: center;
justify-content: center;

img{
  width: 80px;
  height: 50px;
}

`