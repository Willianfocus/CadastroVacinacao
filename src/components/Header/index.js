import React from 'react'
import * as S from './styles'

import Logo from '../../assets/focuslogo.png'

function Header () {
  return(
    <S.Container>
      
      <S.Editlogo>
      <img src={Logo} alt="logo" />
      </S.Editlogo>

    </S.Container>
  )
}

export default Header