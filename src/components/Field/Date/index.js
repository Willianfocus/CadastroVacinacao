import React from 'react'

import Label from '../Label'
import Content from "../content";
import Input from '../Input'

const Date = ({ label, name, register , width, height }) => (
  <Label width={width} height={height} >
     <Content>{ label }</Content>
     <Input type="date"
      name={name}
      />
  </Label>
);

export default Date;
