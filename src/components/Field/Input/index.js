import styled from 'styled-components'

const Input = styled.input`
     background: transparent;
     border: none;
     border-bottom: 2px solid #EE6b26;
     width: 50%;
     height: 2rem;    

      ;
     display: flex;

     text-align: center;

      &:hover{
         color: #444 ;
      }
`

export default Input