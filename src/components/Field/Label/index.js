import styled from 'styled-components'

const Label = styled.label.attrs(props => ({
  size: props.widht || '100%',
}
  )
    )

`

  display: flex;
  flex-direction: column;
  font-size: 20px;
  align-items:center;
  text-align: center;
  width:  ${props => props.size};
  color: #000;
  margin-top: 2rem;
  height: 2rem;

`

export default Label