import React from "react";

import Label from "../Label";
import Content from "../content";
import Input from "../Input";

const Text = ({ label, width, register, rules = {}, ...props }) => (
  <Label width={width}>
    <Content>{label}</Content>

    <Input type="text" width='100%' {...props}  />
  </Label>
);

export default Text;
