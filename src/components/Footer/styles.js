import styled from 'styled-components'

export const Container = styled.div`

  height: 30px;
  background: #20295F;
  border-top: 5px solid #EE6b26;
  width: 100%;

  position: fixed;
  bottom: 0;

  display: flex;
  align-items: center;
  justify-content: center;

  span{
    color: #fff;
    font-size: 20px;
  }

`