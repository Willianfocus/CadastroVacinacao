import React from 'react'
import * as S from './styles'

import GlobalStyle from './styles/global';

import Header from './components/Header'
import Footer from './components/Footer'
import Button from './components/Button'

import Formprincipal from './components/Forms/Formprincipal'

import Field from './components/Field'
import Label from './components/Field/Label'

function App() {
  return (
    <>
      <S.Container>
        <GlobalStyle />

        <Header />

        <S.Stylesform>
          <Formprincipal>

            <S.Editbox>
              <Label>Cartão nacional de saúde</Label>
              <Field.Text></Field.Text>
            </S.Editbox>

            <S.Formbutton>
              <S.Editbutton>
                <Button>Pesquisar</Button>
              </S.Editbutton>
            </S.Formbutton>

          </Formprincipal>
        </S.Stylesform>
        <Footer />
      </S.Container>
    </>
  );
}

export default App;
